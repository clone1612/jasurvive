﻿#pragma strict

// Health of our enemy
var health : int = 100;

function Update()
{
	if (health <= 0)
	{
		DestroyEnemy();
	}
}

function ApplyDamage(damage : int)
{
	health -= damage;
}

function DestroyEnemy()
{
	Destroy(gameObject);
}