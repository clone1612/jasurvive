﻿#pragma strict

// Damage we'll be doing to objects/enemies
var damage : int = 50;
// How far away is our opponent from us?
var distance : float;
// Max distance from us to deal damage
var maxDistance : float = 1.5;
// One of the weapons
var mace : Transform;

function Update()
{
	if (Input.GetButtonDown("Fire1"))
	{
		mace.animation.Play("Attack");
		// Draw a line from our object -> if "hit" give us information about it
		var hit : RaycastHit;
		if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), hit))
		{
			distance = hit.distance;
			if (distance < maxDistance)
			{
				hit.transform.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
}